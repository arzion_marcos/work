<?php
 
namespace App\Http\Controllers;
 
use App\Curriculum;
use App\Skill;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
 
class TestController extends Controller{
 
 
    public function index(Request $request){
 
        //$Curriculums = Curriculum::with('skills')->get();
        /*$skills = Skill::where('title', 'ii')->get();

        if (count($skills)==0) {
            $test = "No hay";
        } else {
            $test = "Hay";
        }*/
 
        return response()->json($request);
 
    }
 
    public function get($id, Request $request){
 
        //$Curriculum  = Profile::with('skills')->get()->find($id);
 
        return response()->json($request);

    }
 
    public function create(Request $request){
        $data = json_decode($request->getContent());
        $Curriculum = new Curriculum;
        $Curriculum->cvUrl = $data->cvUrl ?? '';
        $Curriculum->profileUrl = $data->profileUrl ?? '';
        $Curriculum->save();

        foreach ($data->skills as $key => $value) {
            $Skill = new Skill();
            $Skill->title = $value->title;
            $Skill->save();
            $Curriculum->skills()->attach($Skill->id);
        }
 
        return response()->json($Curriculum); 
    }
 
    public function delete($id){
        $Curriculum  = Curriculum::find($id);
        $Curriculum->delete();

        return response()->json('deleted');
    }
 
    public function update(Request $request,$id){
        $Curriculum  = Curriculum::find($id);
        $data = json_decode($request->getContent());
        $Curriculum->cvUrl = $data->cvUrl ?? $Curriculum->cvUrl;
        $Curriculum->profileUrl = $data->profileUrl ?? $Curriculum->profileUrl;
        $Curriculum->save();

        foreach ($data->skills as $key => $value) {
            $Skill = new Skill();
            $Skill->title = $value->title;
            $Skill->save();
            $Curriculum->skills()->attach($Skill->id);
        }
 
        return response()->json('Ok');
    }

    public function options(Request $request,$id) {
        return response('', 200);
    }
 
}
