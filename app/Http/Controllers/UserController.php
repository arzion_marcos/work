<?php
 
namespace App\Http\Controllers;
 
use App\User;
use App\Profile;
use App\Curriculum;
use App\Skill;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
 
class UserController extends Controller{
 
 
    public function index(){
 
        $Users  = User::all();
 
        return response()->json($Users);
 
    }
 
    public function get($id){ 
        $User  = User::find($id);
        $Profile = Profile::where('user_id', $id)->get();
        $Curriculum = Curriculum::where('user_id', $id)->with('skills')->get();
        if (count($Profile) > 0) {
            $User->profile = $Profile[0];
        }
        if (count($Curriculum) > 0) {
            $User->curriculum = $Curriculum[0];
        } 
        return response()->json($User);
    }
 
    public function create(Request $request){
        $data = json_decode($request->getContent()); 
        $User = new User();
        $User->email = $data->email ?? '';
        $User->validated = $data->validated ?? false;
        $User->role = $data->role ?? 3;
        $User->save();
 
        return response()->json($User); 
    }
 
    public function delete($id){
        $User = User::find($id);
        $User->delete();

        return response()->json('deleted');
    }
 
    public function update(Request $request,$id){
        $User  = User::find($id);
        $data = json_decode($request->getContent()); 
        $User->email = $data->email ?? $User->email;
        $User->validated = $data->validated ?? $User->validated;
        $User->password = $data->password ?? $User->password;
        $User->role = $data->role ?? '';
        $User->save();
 
        return response()->json($User);
    }

    public function options(Request $request,$id) {
        return response('', 200);
    }
 
}