<?php
 
namespace App\Http\Controllers;
 
use App\Profile;
use App\Skill;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
 
class ProfileController extends Controller{
 
 
    public function index(){
        
        $Profiles  = Profile::all();

        return response()->json($Profiles);
 
    }
 
    public function get($id){
 
        $Profile  = Profile::find($id);
 
        return response()->json($Profile);

    }
 
    public function create(Request $request){
        $data = json_decode($request->getContent());
        $Profile = new Profile;
        $Profile->user_id = $data->user_id ?? null;
        $Profile->fullname = $data->fullname ?? '';
        $Profile->description = $data->description ?? '';
        $Profile->birth = $data->birth ?? '';
        $Profile->image = $data->image ?? '';
        $Profile->save();
 
        return response()->json($Profile); 
    }
 
    public function delete($id){
        $Profile  = Profile::find($id);
        $Profile->delete();

        return response()->json('deleted');
    }
 
    public function update(Request $request,$id){
        $Profile  = Profile::find($id);
        $data = json_decode($request->getContent());
        $Profile->fullname = $data->fullname ?? $Profile->fullname;
        $Profile->description = $data->description ?? $Profile->description;
        $Profile->birth = $data->birth ?? $Profile->birth;
        $Profile->image = $data->image ?? $Profile->image;
        $Profile->save();
 
        return response()->json($Profile);
    }

    public function options(Request $request,$id) {
        return response('', 200);
    }
 
}
