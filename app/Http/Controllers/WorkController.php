<?php
 
namespace App\Http\Controllers;
 
use App\Work;
use App\Skill;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
 
 
class WorkController extends Controller{
 
 
    public function index(){
 
        $Works  = Work::with('requiredSkills', 'desirableSkills', 'applicants')->get();

        //$Works  = Work::all();
 
        return response()->json($Works);
 
    }
 
    public function get($id){
 
        $Work  = Work::with('requiredSkills', 'desirableSkills', 'applicants')->find($id);
 
        return response()->json($Work);
    }
 
    public function create(Request $request){ 
        $data = json_decode($request->getContent());
        $Work = new Work;
        $Work->title = $data->title;
        $Work->description = $data->description;
        $Work->state = $data->state;
        $Work->save();

        foreach ($data->required_skills as $key => $value) {
            $skills = Skill::where('title', $value->title)->get();
            //if ($value->id == -1) {
            if (count($skills)==0) {
                $Skill = new Skill();
                $Skill->title = $value->title;
                $Skill->save();
            } else {
                $Skill = Skill::find($value->id);
            };
            $Work->requiredSkills()->attach($Skill->id);
        }

        foreach ($data->desirable_skills as $key => $value) {
            $skills = Skill::where('title', $value->title)->get();
            //if ($value->id == -1) {
            if (count($skills)=='0') {
                $Skill = new Skill();
                $Skill->title = $value->title;
                $Skill->save();
            } else {
                $Skill = Skill::find($value->id);
            };
            $Work->desirableSkills()->attach($Skill->id);
        }
 
        return response()->json($Work);
    }
 
    public function delete($id){
        $Work  = Work::find($id);
        $Work->delete();

        return response()->json('deleted');
    }
 
    public function update(Request $request,$id){
        $Work  = Work::find($id);
        $data = json_decode($request->getContent());
        $Work->title = $data->title ?? $Work->title;
        $Work->description = $data->description ?? $Work->description;
        $Work->state = $data->state ?? $Work->state;
        $Work->save();

        foreach ($data->required_skills as $key => $value) {
            $skills = Skill::where('title', $value->title)->get();
            if (count($skills)==0) {
                $Skill = new Skill();
                $Skill->title = $value->title;
                $Skill->save();
            } else {
                $Skill = Skill::find($value->id);
            }
            $Work->requiredSkills()->attach($Skill->id);
        }

        foreach ($data->desirable_skills as $key => $value) {
            $skills = Skill::where('title', $value->title)->get();
            if (count($skills)==0) {
                $Skill = new Skill();
                $Skill->title = $value->title;
                $Skill->save();
            } else {
                $Skill = Skill::find($value->id);
            }
            $Work->requiredSkills()->attach($Skill->id);
        }
 
        return response()->json($Work);
    }

    public function attachUser(Request $request,$id){
        $Work  = Work::find($id);
        $data = json_decode($request->getContent());

        $Work->applicants()->attach($data->id);
 
        return response()->json($Work);
    }

    public function dettachUser(Request $request,$id, $userid){
        $Work  = Work::find($id);

        $Work->applicants()->detach($userid);
 
        return response()->json($Work);
    }

    public function options(Request $request,$id) {
        return response('', 200);
    }
 
}