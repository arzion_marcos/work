<?php
 
namespace App\Http\Controllers;
 
use App\Skill;
use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
 
 
class SkillController extends Controller{
 
 
    public function index(){
 
        $Skills  = Skill::all();
 
        return response()->json($Skills); 
    }
 
    public function get($id){
 
        $Skill  = Skill::find($id);
 
        return response()->json($Skill);
    }
 
    public function create(Request $request){
        $data = json_decode($request->getContent()); 
        $Skill = new Skill();
        $Skill->title = $data->title;
        $Skill->save();
 
        return response()->json($Skill); 
    }
 
    public function delete($id){
        $Skill  = Skill::find($id);
        $Skill->delete();

        return response()->json('deleted');
    }
 
    public function update(Request $request,$id){
        $Skill  = Skill::find($id);
        $data = json_decode($request->getContent()); 
        $Skill->title = $data->title;
 
        return response()->json($Skill); 
    }

    public function options(Request $request,$id) {
        return response('', 200);
    }
 
}