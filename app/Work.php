<?php

namespace App;

use App\Skill;
use Illuminate\Auth\Authenticatable;
use Laravel\Lumen\Auth\Authorizable;
use Illuminate\Database\Eloquent\Model;

class Work extends Model
{

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'id', 'title', 'description', 'requiredSkills', 'desirableSkills', 'state', 'applicants'
    ];

    /**
     * The attributes excluded from the model's JSON form.
     *
     * @var array
     */
    protected $hidden = [
    
    ];

    public function requiredSkills()
    {
        return $this->belongsToMany('App\Skill', 'works_requiredSkills', 'work_id', 'skill_id');
    }

    public function desirableSkills()
    {
        return $this->belongsToMany('App\Skill', 'works_diserableSkills', 'work_id', 'skill_id');
    }

    public function applicants()
    {
        return $this->belongsToMany('App\User', 'works_users', 'work_id', 'user_id');
    }

}