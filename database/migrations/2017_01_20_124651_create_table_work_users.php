<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWorkUsers extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_users', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('work_id')->unsigned();
            $table->integer('user_id')->unsigned();

            $table->foreign('work_id')->reference('id')->on('works');
            $table->foreign('user_id')->reference('id')->on('users');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        //
    }
}
