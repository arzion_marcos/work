<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableCurriculaSkill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('curriculums_skills', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('curriculum_id')->unsigned();
            $table->integer('skill_id')->unsigned();

            $table->foreign('curriculum_id')->reference('id')->on('curriculums');
            $table->foreign('skill_id')->reference('id')->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('curriculums_skills');
    }
}

