<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableProfileSkill extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('profiles_skills', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('profile_id')->unsigned();
            $table->integer('skill_id')->unsigned();

            $table->foreign('profile_id')->reference('id')->on('profiles');
            $table->foreign('skill_id')->reference('id')->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('profiles_skills');
    }
}
