<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateTableWorkRequirement extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('works_requiredSkills', function (Blueprint $table) {
            $table->increments('id');
            $table->timestamps();
            $table->integer('work_id')->unsigned();
            $table->integer('skill_id')->unsigned();

            $table->foreign('work_id')->reference('id')->on('works');
            $table->foreign('skill_id')->reference('id')->on('skills');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('works_requiredSkills');
    }
}
