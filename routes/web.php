<?php

/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It is a breeze. Simply tell Lumen the URIs it should respond to
| and give it the Closure to call when that URI is requested.
|
*/

$app->get('/', function () use ($app) {
    return $app->version();
});

$app->group(['prefix' => 'users'], function($app)
{
    $app->get('/','UserController@index');
 
    $app->get('/{id}','UserController@get');
     
    $app->post('/','UserController@create');
     
    $app->put('/{id}','UserController@update');
     
    $app->delete('/{id}','UserController@delete');

    $app->options('/{id}', 'UserController@options');

    $app->get('/{id}/profile/{idProfile}','ProfileController@get');
});

$app->group(['prefix' => 'profiles'], function($app)
{
    $app->get('/','ProfileController@index');
 
    $app->get('/{id}','ProfileController@get');
     
    $app->post('/','ProfileController@create');
     
    $app->put('/{id}','ProfileController@update');
     
    $app->delete('/{id}','ProfileController@delete');

    $app->options('/{id}', 'ProfileController@options');
});

$app->group(['prefix' => 'skills'], function($app)
{
    $app->get('/','SkillController@index');
 
    $app->get('/{id}','SkillController@get');
     
    $app->post('/','SkillController@create');
     
    $app->put('/{id}','SkillController@update');
     
    $app->delete('/{id}','SkillController@delete');

    $app->options('/{id}', 'SkillController@options');
});

$app->group(['prefix' => 'works'], function($app)
{
    $app->get('/','WorkController@index');
 
    $app->get('/{id}','WorkController@get');
     
    $app->post('/','WorkController@create');

    $app->post('/{id}/users/','WorkController@attachUser');
     
    $app->put('/{id}','WorkController@update');
     
    $app->delete('/{id}','WorkController@delete');

    $app->delete('/{id}/users/{userId}','WorkController@dettachUser');

    $app->options('/{id}', 'WorkController@options');
});

$app->group(['prefix' => 'curriculums'], function($app)
{
    $app->get('/','CurriculumController@index');
 
    $app->get('/{id}','CurriculumController@get');
     
    $app->post('/','CurriculumController@create');
     
    $app->put('/{id}','CurriculumController@update');
     
    $app->delete('/{id}','CurriculumController@delete');

    $app->options('/{id}', 'CurriculumController@options');

    //$app->get('/{id}/profile/{idProfile}','ProfileController@get');
});

$app->group(['prefix' => 'test'], function($app)
{
    $app->get('/','TestController@index');
 
    $app->get('/{id}','TestController@get');
     
    $app->post('/','TestController@create');
     
    $app->put('/{id}','TestController@update');
     
    $app->delete('/{id}','TestController@delete');

    $app->options('/{id}', 'TestController@options');

    //$app->get('/{id}/profile/{idProfile}','ProfileController@get');
});
